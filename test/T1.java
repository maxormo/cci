import org.junit.Test;

import static org.junit.Assert.*;

public class T1 {

    @Test
    public void t11() throws Exception {
        assertTrue(S1.s11("has ome"));
        assertFalse(S1.s11("not s11 at all"));

    }

    @Test
    public void t12() {
        char[] c1 = "helloo".toCharArray();
        char[] c2 = S1.s12(c1);
        assertTrue("oolleh".equals(new String(c2)));
    }

    @Test
    public void t13() {
        boolean b = S1.s13("hi there", "th ereih");
        assertTrue(b);
    }

    @Test
    public void t14() {
        char[] s = {' ', ' ', 0, 0, 0, 0};
        char[] r = {'%', '2', '0', '%', '2', '0'};

        S1.s14(s, 2);

        assertArrayEquals(r, s);

    }

    @Test
    public void t15() {
        String res = S1.s15("aabbcc");
        assertEquals("aabbcc", res);
        String res1 = S1.s15("aaabbbccc");
        assertEquals("a3b3c3", res1);
    }

}