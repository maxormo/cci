import java.util.Arrays;

/**
 * @author maxormo.
 */
public class S1 {

    /**
     * 1.1 Implement an algorithm to determine if a string has all s11 characters.
     * What if you cannot use additional data structures ?
     */

    public static boolean s11(String input) {
        boolean[] v = new boolean[256];

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (v[c]) {
                return false;
            }
            v[c] = true;
        }
        return true;
    }

    /**
     * Implement a function void reverse(char str ) in C or C++ which reverse s a null - terminated string.
     */
    public static char[] s12(char[] in) {

        int s = in.length;

        for (int i = 0; i < s / 2; i++) {
            char t = in[s - i - 1];
            in[s - i - 1] = in[i];
            in[i] = t;
        }

        return in;
    }


    /**
     * Given two strings, write a method to decide if one is a permutation of the other
     */

    public static boolean s13(String s1, String s2) {
        if (s1.length() == s2.length()) {
            char[] a1 = s1.toCharArray();
            char[] a2 = s2.toCharArray();

            Arrays.sort(a1);
            Arrays.sort(a2);

            return Arrays.equals(a1, a2);
        }
        return false;
    }

    /**
     * Write a method to replace all spaces in a string with '%20'
     */

    public static void s14(char[] s, int len) {
        int spaces = 0, newLength;
        for (char c : s) {
            if (c == ' ') {
                spaces++;
            }
        }

        newLength = len + spaces * 2;

        for (int i = len - 1; i >= 0; i--) {
            if (s[i] == ' ') {
                s[newLength - 1] = '0';
                s[newLength - 2] = '2';
                s[newLength - 3] = '%';
                newLength -= 3;
            } else {
                s[newLength - 1] = s[i];
                newLength--;
            }
        }
    }

    public static String s15(String in) {
        StringBuilder res = new StringBuilder();
        char c = in.charAt(0);
        int cnt = 1;
        for (int i = 1; i < in.length(); i++) {
            if (in.charAt(i) == c) {
                cnt++;
            } else {
                res.append(c).append(cnt);
                c = in.charAt(i);
                cnt = 1;
            }
        }
        res.append(c).append(cnt);

        if (res.length() == in.length()) return in;
        return res.toString();
    }

}
